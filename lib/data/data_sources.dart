import "dart:convert";
import "package:http/http.dart" as http;
import 'dart:math';
import 'dart:async';

import '../utils/comm_utils.dart';


List<String> popBooks = [
  'Pop001.jpg',
  'Pop002.jpg',
  'Pop003.jpg',
  'Pop004.jpg',
  'Pop005.jpg',
  'Pop006.jpg',
  'Pop007.jpg',
  'Pop008.jpg',
  'Pop009.jpg',
  'Pop010.jpg',
  'Pop011.jpg',
  'Pop012.jpg'
];
List<String> techBooks = [
  'C001.JPG',
  'Ceph001.jpg',
  'Compile001.jpg',
  'Compile002.jpg',
  'Compile003.jpg',
  'Computer001.jpg',
  'Computer002.jpg',
  'CSS001.JPG',
  'DataStructure001.jpg',
  'DataStructure002.jpg',
  'DB001.JPG',
  'DB002.JPG',
  'DB003.JPG',
  'DB004.JPG',
  'Game001.jpg',
  'Go001.jpg',
  'Go002.jpg',
  'Go003.jpg',
  'Go004.jpg',
  'Go005.jpg',
  'HTML001.JPG',
  'Java001.jpg',
  'Java002.jpg',
  'Java003.jpg',
  'Java004.jpg',
  'Java005.jpg',
  'Java006.jpg',
  'Java007.jpg',
  'JS001.JPG',
  'js002.jpg',
  'js003.jpg',
  'js004.jpg',
  'Linux001.jpg',
  'Linux002.jpg',
  'Linux003.jpg',
  'Linux004.jpg',
  'Linux005.jpg',
  'Linux006.jpg',
  'Linux007.jpg',
  'MicroService001.jpg',
  'MicroService002.jpg',
  'Network001.jpg',
  'Network002.jpg',
  'Programming001.jpg',
  'Programming002.jpg',
  'Python001.jpg',
  'Python002.jpg',
  'Python003.jpg',
  'Python004.jpg',
  'Python005.jpg',
  'Python006.jpg',
  'Python007.jpg',
  'Python008.jpg',
  'Python009.jpg',
  'Python010.jpg',
  'Python011.jpg',
  'Python012.jpg',
  'Python013.jpg',
  'Python014.jpg',
  'Redis001.jpg',
  'Redis002.jpg',
  'System001.jpg',
  'Virtualization001.jpg',
  'Virtualization002.jpg',
  'Virtualization003.jpg',
  'Virtualization004.jpg',
  'Virtualization005.jpg',
  'Web001.jpg',
  'Web002.jpg',
  'Web003.jpg',
  'Web004.jpg',
  'Web005.jpg',
  'Web006.jpg',
  'Web007.jpg'
];
List<String> allBooks = popBooks + techBooks;
const imagePath = 'lib/images/';
const popBookImagePath = '${imagePath}pop_books/';
const techBookImagePath = '${imagePath}tech_books/';

///Book Data structure definition
class Book {
  String title = "";
  String author = "";
  String cover = "";
  String category = "";
  String details = "";

  Book(
      {this.title="",
      this.author="",
      this.cover="",
      this.category="",
      this.details=""});

/// Choose a book cover picture randomly from local book images path
  String randomBookCover({String category = "all"}) {
    // Book _bk = Book();
    var rand = Random();
    String bookCover;
    try {
      switch (category) {
        case "tech":
          bookCover =
              techBookImagePath + techBooks[rand.nextInt(techBooks.length)];
          break;
        case "pop":
          bookCover =
              popBookImagePath + popBooks[rand.nextInt(popBooks.length)];
          break;
        case "all":
          int index = rand.nextInt(allBooks.length);
          bookCover = ((index < popBooks.length)
                  ? popBookImagePath
                  : techBookImagePath) +
              allBooks[index];
          break;
        default:
          bookCover = '${imagePath}book.jpg';
      }
    } catch (e) {
      bookCover = '${imagePath}book.jpg';
      throw ("Error: BookCover selecting failed...$e");
    }
    return bookCover;
  }
}

/// Fetch JsonData from a URL
Future fetchJsonData(String url) async {
  try {
    final response = await http.get(Uri.parse(url)); //.timeout(Duration(seconds:30));
      if (response.statusCode == 200) {
        final jsonData = (json.decode(response.body) as List).cast<Map<dynamic, dynamic>>();
        return jsonData;
      }
  } catch (e) {
    // print("====>>>>数据获取异常<<<<====......");
    throw Exception('====>>>>数据获取异常<<<<====: Failed to fetch JSON data from $url: $e');
  }
}

Future<List<Book>> createBookListData() async {
  List<Book> bks = <Book>[];
  List<Map<dynamic, dynamic>> jsonData = [];
  final url = "https://jsonplaceholder.typicode.com/posts";
  try {

    final response = await http.get(Uri.parse(url)); //.timeout(Duration(seconds:30));
      if (response.statusCode == 200) {
        jsonData = (json.decode(response.body) as List).cast<Map<dynamic, dynamic>>();
      }
  } catch (e) {
    // print("====>>>>数据获取异常<<<<====......");
    throw Exception('====>>>>数据获取异常<<<<====: Failed to fetch JSON data from $url: $e');
  }
   
    for (var p in jsonData) {
      Book bk = Book();
      String authorName = generateRandomString(Random().nextInt(10)+1, Random().nextInt(12)+1);
      //print("=============jsonData数据源====================\n$p\n+++++++++++++++++++++++");
      bk.title = p['title'];
      bk.author = "${p['userId'].toString()} $authorName";
      bk.category = p['userId'].toString();
      bk.details = p['body'];
      // 目前用随机的方式从现有图库中取出图书封面图片的全路径名
      bk.cover = bk.randomBookCover();
      bks.add(bk);
    }
    // print("<<<<<图书数据初始化结束>>>>>");
    // print("///获取了${bks.length}本图书///");
  return bks;
}


List<T> recommendList<T>({required List<T> list, int num = 1, String category="all"}) {
  var rand = Random();
  List<T> recList = [];
  int randSeed = list.length;
  for(int i=0; i<num; i++){
    recList.add(list[rand.nextInt(randSeed)]);
  }
  
  return recList;


}



Future<List<Book>> fetchNewgoods() async {
  List<Book> tmpBooks = [];
  // print("被上拉动作触发加载新数据");

  List<Book> bs;
  bs = await createBookListData(); 
    
    try {
      // print("=====图书数量====: ${bs.length}");
      tmpBooks = recommendList(list: bs, num: 6);

    } catch (e) {
      // print("====>生成推荐图书失败！<=====");
      throw ("推荐图书失败: Error $e");
    }

  for(Book p in tmpBooks) {
    print("${p.title}, ${p.cover}");
  }

  // print('return之前打印第0本书：${tmpBooks.length}, ${tmpBooks[0].cover}');
  return tmpBooks;  
}
