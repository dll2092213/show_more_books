import 'package:flutter/material.dart';

import '../data/data_sources.dart';

class BookDetails extends StatelessWidget {
  final Book book;

  BookDetails({required this.book});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(book.title),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            margin: EdgeInsets.all(8),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  
                  //spreadRadius: 1.0,
                  blurRadius: 5.0,
                  offset: Offset(3.0, 4.0),
                  color: Colors.black26.withOpacity(0.5),
                ),
              ],
            ),
            child: Padding(
              padding: const EdgeInsets.only(left:0, top: 0,right:5, bottom:5),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: Image.asset(
                  book.cover,
                  width: 330,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              book.details,
              style: TextStyle(fontSize: 18),
            ),
          ),
        ],
      ),
    );
  }
}
