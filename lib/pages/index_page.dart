import 'package:flutter/material.dart';
import 'package:easy_refresh/easy_refresh.dart';

import '../data/data_sources.dart';
import '../utils/pull_up_list_wraper_widget.dart';

List<Map<String, dynamic>> sdList = [];

class IndexPage extends StatefulWidget {
  IndexPage({super.key});

  @override
  State<IndexPage> createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage>
    with AutomaticKeepAliveClientMixin {
  final ScrollController _scrollController = ScrollController();
  List<Book> pickForYouBooks = [];

  @override
  bool get wantKeepAlive => true;
  GlobalKey<RefreshIndicatorState> _easyRefreshKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("↑↑Pull up↑↑ Get More Books..."),
      ),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage("lib/images/fuji_mount.jpg"),
              fit: BoxFit.cover),
        ),
        alignment: Alignment.center,
        child: EasyRefresh(
          key: _easyRefreshKey,
          header: PhoenixHeader(),
          footer: PhoenixFooter(),
          child: ListView(
            controller: _scrollController,
            scrollDirection: Axis.vertical,
            children: [
              PullUpListWraper(
                pickBooks: pickForYouBooks,
              ),
            ],
          ),
          onLoad: () {
            setState(
              () {
                fetchNewgoods().then(
                  (bs) {
                    pickForYouBooks.addAll(bs);
                    // for (var b in pickForYouBooks) {
                    //   print('书名：${b.title}, 封面图片：${b.cover}');
                    // }
                    // print(
                    //     "===========更新后的picBooks有${pickForYouBooks.length}本书===============");
                  },
                );
              },
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        shape: OvalBorder(),
        tooltip: 'Back to Top',
        child: const Icon(Icons.vertical_align_top),
        onPressed: () {
          // print("点击了Back to Top");
          try {
            if (_scrollController.hasClients) {
              // print("我要回到Top啦!");
              _scrollController.animateTo(0.0,
                  duration: const Duration(milliseconds: 500),
                  curve: Curves.linear);
            }
            // else {
            //   print("XXXXXXXXX");
            // }
          } catch (e) {
            // print("回不去Top T-T $e");
            throw ("回不去Top T-T $e");
          }
        },
      ),
    );
  }
}