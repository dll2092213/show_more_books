import 'dart:math';
import 'package:flutter/material.dart';
import 'package:glassmorphism/glassmorphism.dart';

import '../data/data_sources.dart';
import '../pages/book_details.dart';

// generate a Random name String ("FirstName LastName")
String generateRandomString(int fNameLength, int lNameLength) {
  var random = Random();
  const charset = 'abcdefghijklmnopqrstuvwxyz';
  String fName =
      List.generate(fNameLength, (_) => charset[random.nextInt(charset.length)])
          .join();
  String lName =
      List.generate(lNameLength, (_) => charset[random.nextInt(charset.length)])
          .join();
  fName = fName.replaceFirst(fName[0], fName[0].toUpperCase());
  lName = lName.replaceFirst(lName[0], lName[0].toUpperCase());
  //print('$fName $lName');
  return '$fName $lName';
}

/// Card Style
/// +--------------------------+
/// | +-------+                |
/// | |       |  BookName...   |
/// | | Cover |  Author ...    |
/// | |       |  Favor 12345   |
/// | +-------+                |
/// +--------------------------+
class GlassmorpicCard extends StatelessWidget {
  final Book book;
  const GlassmorpicCard({required this.book, super.key});

  @override
  Widget build(BuildContext context) {
    return GlassmorphicContainer(
      //毛玻璃效果
      width: 300,
      height: 180,
      margin: EdgeInsets.only(bottom: 8),
      borderRadius: 10,
      blur: 10,
      alignment: Alignment.topLeft,
      border: 2,
      linearGradient: LinearGradient(
        begin: Alignment.centerLeft,
        end: Alignment.bottomRight,
        colors: [
          Color(0xFFffffff).withOpacity(0.1),
          Color(0xFFFFFFFF).withOpacity(0.05),
        ],
        stops: [
          0.1,
          1,
        ],
      ),
      borderGradient: LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [
          Color(0xFFffffff).withOpacity(0.5),
          Color((0xFFFFFFFF)).withOpacity(0.5),
        ],
      ),
      child: InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => BookDetails(book: book),
            ),
          );          
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.max,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: Image.asset(
                  //Book Cover
                  book.cover,
                  width: 120,
                  height: 170,
                  fit: BoxFit.fitWidth,
                ),
              ),
              SizedBox(
                width: 150,
                height: 170,
                child: Padding(
                  padding: const EdgeInsets.only(left: 5.0, top: 18.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        //Book Name
                        book.title,
                        style: const TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                          overflow: TextOverflow.ellipsis,
                        ),
                        maxLines: 1,
                      ),
                      Text(
                        //Author
                        book.author,
                        style: const TextStyle(
                          fontSize: 13,
                          fontStyle: FontStyle.italic,
                          overflow: TextOverflow.ellipsis,
                        ),
                        maxLines: 1,
                      ),
                      Row(
                        children: [
                          const Icon(
                            Icons.favorite,
                            color: Colors.red,
                          ),
                          Text(
                            (Random().nextInt(9999) + 400).toString(),
                            style: TextStyle(
                              fontSize: 15,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


