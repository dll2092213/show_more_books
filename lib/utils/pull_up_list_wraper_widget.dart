import 'package:flutter/material.dart';

import '../utils/comm_utils.dart';
import '../data/data_sources.dart';

class PullUpListWraper extends StatefulWidget {
  List<Book> pickBooks = [];
  PullUpListWraper(
      {required this.pickBooks, Key? key})
      : super(key: key);

  @override
  State<PullUpListWraper> createState() => _PullUpListWraperState();
}

class _PullUpListWraperState extends State<PullUpListWraper> {
  List<GlassmorpicCard> bookCards = [];
  List<Book> _pickBooks = [];
 
  @override
  void initState() {
    super.initState();

    _pickBooks = widget.pickBooks;
    List<Book> tmpBooks = [];
    // print("重新加载HomePage数据...");
    createBookListData().then((bs) {
        try {
          // print("=====图书数量====: ${bs.length}");
          setState(
            () {
              // 调用reccomendBooks方法，从图书列表中取出5本书作为推荐图书
              tmpBooks = recommendList(list: bs, num: 6);
              // tmpBooks = renameBookName(tmpBooks);
              // for (var b in tmpBooks) {
              //   print('书名：${b.title}, 封面图片：${b.cover}');
              // }
            },
          );
        } catch (e) {
          // print("====>生成推荐图书失败！<=====");
          throw ("推荐图书失败: Error $e");
        }
        _pickBooks.addAll(tmpBooks);
        // print('_pickBooks.length: ${pickBooks.length}\npickBooks.length: ${pickBooks.length}');
 
    });
 
  }



  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: 1,
      children: _wrapList(_pickBooks),
    );
  }

  ///返回一个_wrapList，用于
  List<Widget> _wrapList<T>(List<T> inputList) {
    List<Widget> listWidget = [];

    if (inputList.isNotEmpty) {
      if (T == Book) {
        //传入Book类型元素，生成CommonCard1类型的Widget列表用于展示
        for (Book bk in inputList as List<Book>) {
          GlassmorpicCard card = GlassmorpicCard(book: bk);
          listWidget.add(card);
        }
      } else {
        Placeholder;
      }
      return listWidget;
    } else {
      print("Error: Empty inputList");
      return listWidget;
    }
  }
}
